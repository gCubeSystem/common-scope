This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for common scope

## [v2.0.0]

- Maps scanned with reflection
- Dependency to common-configuration-scanner removed
- Removed ScopePorvider and ScopeBean

