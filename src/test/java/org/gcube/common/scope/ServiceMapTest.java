package org.gcube.common.scope;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.StringReader;
import java.io.StringWriter;

import org.gcube.common.scope.api.ServiceMap;
import org.gcube.common.scope.impl.DefaultServiceMap;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.junit.After;
import org.junit.Test;

import jakarta.xml.bind.JAXBContext;

public class ServiceMapTest {

	
	
	@Test
	public void serviceMapsBindCorrectly() throws Exception {

		String map = 
				"<service-map scope='scope' version='1.0'>" 
					+ "<services>"
						+ "<service name='service1' endpoint='http://acme.org:8000/service1' />"
						+ "<service name='service2' endpoint='http://acme2.org:8000/service2' />"
					+ "</services>" 
				+ "</service-map>";
		
		JAXBContext context = JAXBContext.newInstance(DefaultServiceMap.class);

		DefaultServiceMap serviceMap = (DefaultServiceMap) context
				.createUnmarshaller().unmarshal(new StringReader(map));

		assertEquals("scope", serviceMap.scope());
		
		assertEquals("1.0", serviceMap.version());

		assertEquals("http://acme.org:8000/service1", serviceMap.endpoint("service1"));
		
		assertEquals("http://acme2.org:8000/service2", serviceMap.endpoint("service2"));
		
		StringWriter sw = new StringWriter();
		context.createMarshaller().marshal(serviceMap, sw);
		
		DefaultServiceMap serviceMapCopy = (DefaultServiceMap) context
				.createUnmarshaller().unmarshal(new StringReader(sw.toString()));
		assertEquals(serviceMapCopy, serviceMap);

	}
	
	@Test
	public void serviceMapsDiscoveredCorrectly() throws Exception {
		
		SecretManagerProvider.set(new TestSecret("/infra/vo"));

		assertNotNull(ServiceMap.instance.endpoint("service1"));
		
		assertEquals("2.3",ServiceMap.instance.version());
		
	}
	
	@Test
	public void serviceMapsCanBeLookedupInVREScope() throws Exception {
		
		SecretManagerProvider.set(new TestSecret("/infra/vo/vre"));

		assertNotNull(ServiceMap.instance.endpoint("service1"));
		
	}
	
	@After
	public void cleanup() {
		SecretManagerProvider.reset();
	}
	
}
