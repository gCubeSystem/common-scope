package org.gcube.common.scope;

import java.util.Collections;
import java.util.Map;

import org.gcube.common.security.Owner;
import org.gcube.common.security.secrets.Secret;

public class TestSecret extends Secret {

	String context;
	
	public TestSecret(String context){
		this.context = context;
	}
	
	@Override
	public Owner getOwner() {
		return new Owner("test", Collections.emptyList(), false, false);
	}

	@Override
	public String getContext() {
		return this.context;
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		return Collections.emptyMap();
	}

	@Override
	public boolean isValid() {
		return true;
	}
	
	@Override
	public boolean isExpired() {
		return false;
	}
}
