package org.gcube.common.scope;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.gcube.common.security.AuthorizedTasks;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.junit.Test;

public class BindingTest {

	private ExecutorService executor = Executors.newSingleThreadExecutor();
	
	
	@Test
	public void callablesAreBound() throws Exception {
		
		final String callScope = "somescope";
		
		SecretManagerProvider.set(new TestSecret(callScope));
		
		Callable<Void> unbound = new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				assertEquals("task thread is not bound to call scope, but to "+SecretManagerProvider.get().getContext(),callScope,SecretManagerProvider.get().getContext());
				return null;
			}
		};
		
		Callable<Void> bound = AuthorizedTasks.bind(unbound);
		
		String newScope = "newscope";
		
		//scope in current thread changes
		SecretManagerProvider.set(new TestSecret(newScope));
		
		//task is submittted
		executor.submit(bound).get();
		
		//resetting task
		assertEquals("call thread does not retain its latest scope",newScope,SecretManagerProvider.get().getContext());

		//reset call scope
		SecretManagerProvider.reset();
		
		Callable<Void> cleanupTest = new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				assertNull(SecretManagerProvider.get());
				return null;
			}
		};
		
		executor.submit(cleanupTest).get();
		
	}
	

	
}
